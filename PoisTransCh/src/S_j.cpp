#include <Rcpp.h>
#include <math.h>
using namespace Rcpp;

// negative binomial offspring distribution:
// the n-th Taylor coefficient of the 0-th Taylor coefficient of S
double S_0_lth(unsigned int l, 
               double R0, 
               double k,
               double p) {
  
  double t;
  double pre_term = (1-p)*pow(1 + R0/k,-k);
  double prod_term = 1;
  if ( l > 1 ) {
    double fac = (R0/k)*(1-p)*pow(1 + R0/k, -(k+1));
    for ( int i=1; i<=l-1; i++) {
      double cur_term = (k*l+i-1)/(i+1)*fac;
      prod_term = prod_term*cur_term;
    }
  }
  t = pre_term*prod_term;
  
  return(t);
}

// the n-th Taylor coefficient of the j-th Taylor coefficient of s
double S_j_lth(unsigned int l, 
               int j, 
               double R0, 
               double k, 
               double p) {
  
  double t;
  
  if (j == 0) {
    t = S_0_lth(l, R0, k, p);
  }
  else {
    if ( l-j==0 ) {
      double pre_term = p*pow(1+R0/k, -k);
      double prod = 1;
      if (l > 1) {
        double fac = R0/k*p*pow(1+R0/k, -(k+1));
        for (int i=1; i<=l-1; i++) {
          double cur_term = fac*(k*l+i-1);
          prod = prod*cur_term;
        }
      }
      t = pre_term*prod;
    }
    else {
      double pre_term = (1-p)*pow(1+R0/k, -k);
      double fac1 = R0/k*p*pow(1+R0/k, -(k+1));
      double prod = 1;
      double cur_term;
      for (int i=1; i<=j; i++) {
        cur_term = fac1*(k*l+i-1);
        prod = prod*cur_term;
      }
      if ( l-j>=2 ) {
        double fac2 = R0/k*(1-p)*pow(1+R0/k, -(k+1));
        for (int i=1; i <= l-j-1; i++) {
          cur_term = fac2*(k*l+j+i-1)/(i+1);
          prod = prod*cur_term;
        }
      }
      t = pre_term*prod;
    }
  }
  return(t);
}

// the 0-th Taylor coefficient of S 
double S_0(double R0,
           double k,
           double p,
           int max_terms = -1,
           double tol = 1e-32) {
  
  double s = 0;
  double cur_term = S_0_lth(1, R0, k, p);
  unsigned int count = 0;
  
  //if max_terms is negative -> treat it as infinity
  while (!isnan(cur_term) && !isinf(cur_term) && fabs(cur_term) >= tol && (max_terms < 0 || count < max_terms)) {
    s += cur_term;
    count += 1;
    cur_term = S_0_lth(count+1, R0, k, p);
  }
  return(s);
}

// the j-th Taylor coefficient of S 
// [[Rcpp::export]]
double S_j(int j,
           double R0,
           double k,
           double p,
           int max_terms = -1,
           double tol = 1e-32) {
  
  double s = 0;
  if (j == 0) {
    s = S_0(R0, k, p,max_terms,tol);
  }
  else {
    double cur_term = S_j_lth(j,j,R0,k,p);
    unsigned int count = 0;
    
    //if max_terms is negative -> treat it as infinity
    while (!isnan(cur_term) && !isinf(cur_term) && fabs(cur_term) >= tol && (max_terms < 0 || count < max_terms)) {
      s += cur_term;
      count += 1;
      cur_term = S_j_lth(j+count,j,R0, k, p);
    }
  }
  return(s);
}

//The derivatives using the finite difference approximations
//First order derivative: with central difference on the log scale
//wrt R0
// [[Rcpp::export]]
double dS_jdR0(int j,
               double R0,
               double k,
               double p,
               double h = 1e-6,
               int max_terms = -1,
               double tol = 1e-32) {
  
  double df_forward = S_j(j,R0*exp(h/2),k,p,max_terms,tol);
  double df_backward = S_j(j,R0*exp(-h/2),k,p,max_terms,tol);
  double df = df_forward/h -df_backward/h;
  
  double s = df * 1/R0;
  
  return(s);
}

//wrt k
// [[Rcpp::export]]
double dS_jdk(int j,
              double R0,
              double k,
              double p,
              double h = 1e-6,
              int max_terms = -1,
              double tol = 1e-32) {
  
  double k_forward = k*exp(h/2);
  double k_backward = k*exp(-h/2);
  double df_forward = S_j(j,R0,k=k_forward,p,max_terms,tol);
  double df_backward = S_j(j,R0,k_backward,p,max_terms,tol);
  double df = df_forward/h - df_backward/h;
  
  double s = df * 1/k;
  
  return(s);
}

//Second order derivative: with central difference on the log scale
//wrt R0 and R0
// [[Rcpp::export]]
double d2S_jdR02(int j,
               double R0,
               double k,
               double p,
               double h = 1e-4,
               int max_terms = -1,
               double tol = 1e-32) {
  
  double df_2h_p = S_j(j,R0*exp(h),k,p,max_terms,tol);
  double df_2h_m = S_j(j,R0*exp(-h),k,p,max_terms,tol);
  double df_h_p = S_j(j,R0*exp(h/2),k,p,max_terms,tol);
  double df_h_m = S_j(j,R0*exp(-h/2),k,p,max_terms,tol);
  double df_0 = S_j(j,R0,k,p,max_terms,tol);
  
  double df = df_2h_p*pow(h,-2) - 2*df_0*pow(h,-2) + df_2h_m*pow(h,-2) - df_h_p/h + df_h_m/h;
  
  double s = df * pow(R0,-2);
  
  return(s);
}

//wrt k and k
// [[Rcpp::export]]
double d2S_jdk2(int j,
                 double R0,
                 double k,
                 double p,
                 double h = 1e-4,
                 int max_terms = -1,
                 double tol = 1e-32) {
  
  double df_2h_p = S_j(j,R0,k*exp(h),p,max_terms,tol);
  double df_2h_m = S_j(j,R0,k*exp(-h),p,max_terms,tol);
  double df_h_p = S_j(j,R0,k*exp(h/2),p,max_terms,tol);
  double df_h_m = S_j(j,R0,k*exp(-h/2),p,max_terms,tol);
  double df_0 = S_j(j,R0,k,p,max_terms,tol);
  
  double df = df_2h_p*pow(h,-2) - 2*df_0*pow(h,-2) + df_2h_m*pow(h,-2) - df_h_p/h + df_h_m/h;
  
  double s = df * pow(k,-2);
  
  return(s);
}

//wrt R0 and k
// [[Rcpp::export]]
double d2S_jdR0dk(int j,
                double R0,
                double k,
                double p,
                double h = 1e-4,
                int max_terms = -1,
                double tol = 1e-32) {
  
  double df_R0k_p = S_j(j,R0*exp(h),k*exp(h),p,max_terms,tol);
  double df_R0k_m = S_j(j,R0*exp(-h),k*exp(-h),p,max_terms,tol);
  double df_R0_p_k_m = S_j(j,R0*exp(h),k*exp(-h),p,max_terms,tol);
  double df_R0_m_k_p = S_j(j,R0*exp(-h),k*exp(h),p,max_terms,tol);
  
  double h2 = 4*pow(h,2);
  
  double df = df_R0k_p/h2 - df_R0_p_k_m/h2 - df_R0_m_k_p/h2 + df_R0k_m/h2;
  
  double s = df / R0 / k;
  
  return(s);
}
