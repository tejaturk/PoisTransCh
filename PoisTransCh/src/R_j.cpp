#include <Rcpp.h>
#include <math.h>
using namespace Rcpp;

// the n-th Taylor coefficient of the 0-th Taylor coefficient of R
double R_0_nth(unsigned int n, 
               double lambda, 
               double p, 
               double rho) {
  
  double t;
  if ( n == 1 ) {
    t = rho*exp(-lambda*(rho-1))*pow(rho,-1)*exp(-lambda)*(1-p);
  }
  else {
    double pre_term = rho*exp(-lambda*(rho-1))*n*lambda*exp(-2*lambda)*pow(1-p,2)/2;
    double prod_term = 1;
    if (n > 2) {
      double fac = (n+rho-1)*lambda*exp(-lambda)*(1-p);
      for ( int i=1; i<=n-2; i++) {
        double cur_term = fac/(i+2);
        prod_term = prod_term*cur_term;
      }
    }
    t = pre_term*prod_term;
  }
  return(t);
}

// the n-th Taylor coefficient of the j-th Taylor coefficient of R
double R_j_nth(unsigned int n, 
               int j, 
               double lambda, 
               double p, 
               double rho) {
  
  double t;
  
  if (j == 0) {
    t = R_0_nth(n, lambda, p, rho);
  }
  else {
    if ( n-j == 0 ) {
      t = rho*pow(p,j)*j*pow(j+rho-1,j-2)*pow(lambda,j-1)*exp(-lambda*(j+rho-1));
    }
    else {
      double pre_term = rho*pow(p,j)*exp(-lambda*(rho-1))*n*pow(n+rho-1,j-2)*pow(lambda,j-1)*exp(-(j*lambda));
      double prod_term = 1;
      double fac = (n+rho-1)*lambda*exp(-lambda)*(1-p);
      for ( int i=1; i<=n-j; i++) {
        double cur_term = fac/i;
        prod_term = prod_term*cur_term;
      }
      t = pre_term*prod_term;
    }
  }
  return(t);
}

// the 0-th Taylor coefficient of R 
double R_0(double lambda,
           double p,
           double rho,
           int max_terms = -1,
           double tol = 1e-32) {
  
  double r = 0;
  double cur_term = R_0_nth(1, lambda, p, rho);
  unsigned int count = 0;
  
  //if max_terms is negative -> treat it as infinity
  while (!isnan(cur_term) && !isinf(cur_term) && fabs(cur_term) >= tol && (max_terms < 0 || count < max_terms)) {
    r += cur_term;
    count += 1;
    cur_term = R_0_nth(count+1,lambda,p,rho);
  }
  return(r);
}

// the j-th Taylor coefficient of R 
// [[Rcpp::export]]
double R_j(int j,
           double lambda,
           double p,
           double rho,
           int max_terms = -1,
           double tol = 1e-32) {
  
  double r = 0;
  if (j == 0) {
    r = R_0(lambda,p,rho,max_terms,tol);
  }
  else {
    double cur_term = R_j_nth(j,j,lambda,p,rho);
    unsigned int count = 0;
    
    //if max_terms is negative -> treat it as infinity
    while (!isnan(cur_term) && !isinf(cur_term) && fabs(cur_term) >= tol && (max_terms < 0 || count < max_terms)) {
      r += cur_term;
      count += 1;
      cur_term = R_j_nth(j+count,j,lambda,p,rho);
    }
  }
  return(r);
}
