#include <Rcpp.h>
#include <math.h>
using namespace Rcpp;


double d2R_0_nth(unsigned int n, 
                 double lambda, 
                 double p, 
                 double rho) {
  
  double t;
  if (n == 1) {
    t = pow(rho,2)*exp(-rho*lambda)*(1-p);
  }
  else if (n == 2) {
    t = rho*(rho+1)*exp(-lambda*(rho+1))*(lambda*(rho+1)-2)*pow(1-p,2);
  }
  else {
    double pre_term = rho*exp(-lambda*(rho-1))*n*(n+rho-1)*exp(-3*lambda)*(pow(lambda,2)*pow(n+rho-1,2)-2*lambda*(n-1)*(n+rho-1)+pow(n,2)-3*n+2)*pow(1-p,3)/6;
    double prod_term = 1;
    if (n > 3) {
      double fac = (n+rho-1)*lambda*exp(-lambda)*(1-p);
      for ( int i=1; i<=n-3; i++) {
        double cur_term = fac/(i+3);
        prod_term = prod_term*cur_term;
      }
    }
    t = pre_term*prod_term;
  }
  return(t);
}

double d2R_j_nth(unsigned int n, 
                 int j, 
                 double lambda, 
                 double p, 
                 double rho) {
  
  double t;
  
  if (j == 0) {
    t = d2R_0_nth(n, lambda, p, rho);
  }
  else {
    if ( n-j == 0 ) {
      t = rho*exp(-lambda*(j+rho-1))*j*pow(j+rho-1,j-2)*pow(lambda,j-3)*(pow(lambda,2)*pow(j+rho-1,2)-2*lambda*(j-1)*(j+rho-1)+pow(j,2)-3*j+2)*pow(p,j);
    }
    else if (n-j == 1) {
      t = rho*exp(-lambda*(j+rho))*(j+1)*pow(j+rho,j-1)*pow(lambda,j-2)*(pow(lambda,2)*pow(j+rho,2)-2*lambda*j*(j+rho)+pow(j,2)-j)*pow(p,j)*(1-p);
    }
    else {
      double pre_term = rho*pow(p,j)*exp(-lambda*(rho-1))*n*pow(n+rho-1,j-2)*pow(lambda,j-3)*(pow(lambda,2)*pow(n+rho-1,2) - 2*lambda*(n-1)*(n+rho-1) + pow(n,2)-3*n+2)*exp(-(j*lambda));
      double prod_term = 1;
      double fac = (n+rho-1)*lambda*exp(-lambda)*(1-p);
      for ( int i=1; i<=n-j; i++) {
        double cur_term = fac/i;
        prod_term = prod_term*cur_term;
      }
      t = pre_term*prod_term;
    }
  }
  return(t);
}

double d2R_0(double lambda,
             double p,
             double rho,
             int max_terms = -1,
             double tol = 1e-32) {
  
  double r = 0;
  double cur_term = d2R_0_nth(1, lambda, p, rho);
  unsigned int count = 0;
  
  //if max_terms is negative -> treat it as infinity
  while (!isnan(cur_term) && !isinf(cur_term) && fabs(cur_term) >= tol && (max_terms < 0 || count < max_terms)) {
    r += cur_term;
    count += 1;
    cur_term = d2R_0_nth(count+1,lambda,p,rho);
  }
  return(r);
}

// [[Rcpp::export]]
double d2R_j(int j,
             double lambda,
             double p,
             double rho,
             int max_terms = -1,
             double tol = 1e-32) {
  
  double r = 0;
  if (j == 0) {
    r = d2R_0(lambda,p,rho,max_terms,tol);
  }
  else {
    double cur_term = d2R_j_nth(j,j,lambda,p,rho);
    unsigned int count = 0;
    
    //if max_terms is negative -> treat it as infinity
    while (!isnan(cur_term) && !isinf(cur_term) && fabs(cur_term) >= tol && (max_terms < 0 || count < max_terms)) {
      r += cur_term;
      count += 1;
      cur_term = d2R_j_nth(j+count,j,lambda,p,rho);
    }
  }
  return(r);
}
