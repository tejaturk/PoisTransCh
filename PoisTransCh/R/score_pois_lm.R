score_pois_lm <- function(beta, X, j, p, rho, max_terms=Inf, tol=1e-32) {
  
  # to call C++ function, the max_terms should be -1 if infinite
  if (is.infinite(max_terms)) {
    max_terms <- -1
  }
  
  # if p is the same for all chains
  if (length(p)==1) {
    p <- rep(p, nrow(X))
  }
  
  # if rho is the same for all chains
  if (length(rho)==1) {
    rho <- rep(rho, nrow(X))
  }
  
  # calculate lambdas: exp(x^T beta) for each row of B
  lambda <- exp(X%*%beta)
  
  # the R derivatives
  Rj <- mapply(FUN = function(ll, jj, pp, aa) R_j(j=jj, 
                                                  lambda=ll, 
                                                  p=pp, 
                                                  rho=aa,
                                                  max_terms = max_terms, tol = tol),
               lambda, j, p, rho)
  # the probability of observing a chain of size 0
  prob0 <- mapply(FUN = function(ll, pp, aa) R_j(j=0,
                                                 lambda=ll, 
                                                 p=pp, 
                                                 rho=aa,
                                                 max_terms = max_terms, tol = tol),
                  lambda, p, rho)
  # the partial derivatives of the R derivatives
  dRj <- mapply(FUN = function(ll, jj, pp, aa) dR_j(j=jj, 
                                                    lambda=ll, 
                                                    p=pp, 
                                                    rho=aa,
                                                    max_terms = max_terms, tol = tol),
                lambda, j, p, rho)
  # the partial derivative of the R0
  dprob0 <- mapply(FUN = function(ll, pp, aa) dR_j(j=0,
                                                   lambda=ll, 
                                                   p=pp, 
                                                   rho=aa,
                                                   max_terms = max_terms, tol = tol),
                   lambda, p, rho) 
  
  # the score
  uu <- array(as.vector((dRj/Rj+dprob0/(1-prob0))*lambda)*X, dim=dim(X))
  u <- array(apply(uu, MARGIN = 2, FUN=sum), dim=c(dim(X)[2]))
  
  dimnames(u)[1] <- dimnames(X)[2]
  
  return(u)
  
}