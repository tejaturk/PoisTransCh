lr_test <- function(mle1, mle2) {
  lr_stat <- lik_ratio(mle1, mle2)
  p_val <- pchisq(q=lr_stat, df=attr(lr_stat, "q"), lower.tail = FALSE)
  attr(p_val, "LR") <- as.numeric(lr_stat)
  attr(p_val, "q") <- attr(lr_stat, "q")
  return(p_val)
}