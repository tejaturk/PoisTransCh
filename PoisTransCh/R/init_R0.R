init_R0 <- function(p, rho, mu, tol=1e-3) {
  a <- function(p, rho, mu) {
    aa <- rho*(mu*(p-1)+p)+p*(mu-1)
    # aa <- (rho-p)*(1-mu)+rho*p*mu
    return(aa)
  }
  
  b <- function(p, rho, mu) {
    bb <- 4*rho*(mu-1)*mu*(1-p)*p + (rho*mu + p - (rho+mu+rho*mu)*p)^2
    # bb <- rho^2*(1-mu*(1-p))^2 - 2*rho*(1-mu)*(1+mu*(1-p))*p + (1-mu)^2*p^2
    return(bb)
  }
  
  C <- function(p, rho, mu) {
    cc <- -2*rho*mu*(1-p)
    return(cc)
  }
  
  aa <- a(p, rho, mu)
  bb <- b(p, rho, mu)
  cc <- C(p, rho, mu)
  
  lambda <- suppressWarnings((aa + c(-1,1)*sqrt(bb))/cc)
  lambda <- lambda[which(lambda > 0 & lambda < 1)]
  # lambda <- lambda[which(lambda > 0 & lambda <= 1)]
  
  if (length(lambda)==0) {
    lambda <- (mu/p-1)/(rho + (mu/p-1))
    # lambda <- (mu-p)/(rho+mu-p)
  }
  
  lambda <- lambda[which(lambda >= 0 & lambda <=1)]
  #if (lambda==0) {
   # lambda <- tol
  #}
  lambda <- max(lambda, tol)

  return(lambda)
  
}