setGeneric("Fisher_info", function(beta, X, j, p=1, rho=1, kappa=Inf,
                             h_1 = 1e-6, h_2 = 1e-4,
                             max_terms=Inf, tol=1e-32) standardGeneric("Fisher_info"))

# beta -> paramaters for R0 on the log-scale
# X -> design matrix for beta
# j -> the corresponding chain sizes
# p -> vector of corresponding p's
# rho -> vector of corresponding rho's
# kappa -> log of k paramater in the negative binomial model. If Inf, the Poisson model is assumed.
# h_1 -> used for the first order numerical derivatives in the case of negative binomial
# h_2 -> used for the second order numerical derivatives in the case of negative binomial

setMethod("Fisher_info", signature(beta="numeric", X="matrix", j="integer", p="numeric", rho="numeric", kappa="numeric", 
                                   h_1="numeric", h_2="numeric",
                             max_terms="numeric", tol="numeric"),
          function(beta, X, j, p, rho, kappa, h_1, h_2, max_terms, tol) {
            # is it Poisson model?
            if (is.infinite(kappa)) {
              # is it a simple model with single p, rho and only intercept?
              if (dim(X)[2]==1 && unique(X)==1 && length(unique(p))==1 && length(unique(rho))==1) {
                # what are the frequencies of the chain sizes?
                j <- table(j)
                n_j <- as.integer(j) # frequencies
                j <- as.integer(names(j)) # chain sizes in the compact form
                
                FI <- Fisher_info_pois_1(beta=beta, j=j, n_j=n_j, 
                                  p=unique(p), rho=unique(rho), 
                                  max_terms=max_terms, tol=tol)
              }
              else {
                FI <- Fisher_info_pois_lm(beta=beta, X=X, j=j, 
                                   p=p, rho=rho, 
                                   max_terms=max_terms, tol=tol)
              }
            }
            # otherwise: it's negative binomial model:
            else {
              # is it a simple model with single p and only intercept?
              if (dim(X)[2]==1 && unique(X)==1 && length(unique(p))==1) {
                # what are the frequencies of the chain sizes?
                j <- table(j)
                n_j <- as.integer(j) # frequencies
                j <- as.integer(names(j)) # chain sizes in the compact form
                
                FI <- Fisher_info_negbin_1(beta=beta, kappa=kappa, j=j, n_j=n_j, 
                                     p=unique(p), h_1=h_1, h_2=h_2, 
                                     max_terms=max_terms, tol=tol)
              }
              else {
                FI <- Fisher_info_negbin_lm(beta=beta, kappa=kappa, X=X, j=j, 
                                      p=p, h_1=h_1, h_2=h_2,
                                      max_terms=max_terms, tol=tol)
              }
            }
            return(FI)
          })