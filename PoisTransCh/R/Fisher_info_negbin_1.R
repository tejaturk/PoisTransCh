Fisher_info_negbin_1 <- function(beta, kappa, j, n_j, p,
                           h_1 = 1e-6, h_2 = 1e-4,
                           max_terms=Inf, tol=1e-32) {

  # to call C++ function, the max_terms should be -1 if infinite
  if (is.infinite(max_terms)) {
    max_terms <- -1
  }
  
  # the probability generating function is defined for R0 = e^beta and k=e^kappa
  R0 <- exp(beta)
  k <- exp(kappa)
  
  # the S derivatives
  Sj <- sapply(j, S_j, R0=R0, k=k, p=p, max_terms=max_terms, tol=tol)
  # the probability of observing a chain of size0
  prob0 <- S_j(j=0, R0=R0, k=k, p=p, max_terms=max_terms, tol=tol) 
  # the partial derivatives of the S derivatives
  dSjR0 <- sapply(j, dS_jdR0, R0=R0, k=k, p=p, h=h_1, max_terms=max_terms, tol=tol)
  dSjk <- sapply(j, dS_jdk, R0=R0, k=k, p=p, h=h_1, max_terms=max_terms, tol=tol)
  # the partial derivative of the S0
  dprob0R0 <- dS_jdR0(j=0, R0=R0, k=k, p=p, h=h_1, max_terms=max_terms, tol=tol)
  dprob0k <- dS_jdk(j=0, R0=R0, k=k, p=p, h=h_1, max_terms=max_terms, tol=tol)
  # the second order partial derivatives of the S derivatives
  d2SjR0 <- sapply(j, d2S_jdR02, R0=R0, k=k, p=p, h=h_2, max_terms=max_terms, tol=tol)
  d2Sjk <- sapply(j, d2S_jdk2, R0=R0, k=k, p=p, h=h_2, max_terms=max_terms, tol=tol)
  d2SjR0k <- sapply(j, d2S_jdR0dk, R0=R0, k=k, p=p, h=h_2, max_terms=max_terms, tol=tol)
  # the second partial derivative of the S0
  d2prob0R0 <- d2S_jdR02(j=0, R0=R0, k=k, p=p, h=h_2, max_terms=max_terms, tol=tol)
  d2prob0k <- d2S_jdk2(j=0, R0=R0, k=k, p=p, h=h_2, max_terms=max_terms, tol=tol)
  d2prob0R0k <- d2S_jdR0dk(j=0, R0=R0, k=k, p=p, h=h_2, max_terms=max_terms, tol=tol)
  
  # the Fisher information matrix
  # the R0 part
  FI_R0 <- -sum(n_j*(((d2SjR0/Sj - (dSjR0/Sj)^2 + d2prob0R0/(1-prob0) + (dprob0R0/(1-prob0))^2)*R0
                      + dSjR0/Sj + dprob0R0/(1-prob0))*R0))
  # prepare array:
  FI_R0 <- array(FI_R0, dim=c(1,1))
  dimnames(FI_R0)[[1]] <- list("(Intercept)")
  dimnames(FI_R0)[[2]] <- list("(Intercept)")
  
  # the k part
  FI_k <- -sum(n_j*(((d2Sjk/Sj - (dSjk/Sj)^2 + d2prob0k/(1-prob0) + (dprob0k/(1-prob0))^2)*k
                      + dSjk/Sj + dprob0k/(1-prob0))*k))
  # prepare array:
  FI_k <- array(FI_k, dim=c(1,1))
  dimnames(FI_k)[[1]] <- list("log(k)")
  dimnames(FI_k)[[2]] <- list("log(k)")
  
  # the mixed part
  FI_R0k <- -sum(n_j*(d2SjR0k/Sj - dSjR0*dSjk/Sj^2 + d2prob0R0k/(1-prob0) + dprob0R0*dprob0k/(1-prob0)^2)*R0*k)
  # prepare array:
  FI_R0k <- array(FI_R0k, dim=c(1,1))
  dimnames(FI_R0k)[[1]] <- list("(Intercept)")
  dimnames(FI_R0k)[[2]] <- list("log(k)")
  
  # together:
  FI <- array(rbind(cbind(FI_R0, FI_R0k), cbind(t(FI_R0k), FI_k)), dim=c(2,2))
  dimnames(FI)[[1]] <- c("(Intercept)", "log(k)")
  dimnames(FI)[[2]] <- c("(Intercept)", "log(k)")
  
  return(FI)
  
}