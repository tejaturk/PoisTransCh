model_selection <- function(mle_fit, criterion="AIC", direction="both",
                            null_model=~1, verbose=FALSE) {
  
  # backward selection
  # remove one-by-one the terms of the formula to see which one reduces the AIC/BIC at most
  if (!(criterion %in% c("AIC", "BIC"))) {
    stop('criterion should be either "AIC" or "BIC"!')
  }
  if (!(direction %in% c("backward", "forward", "both"))) {
    stop('direction should be "backward", "forward" or "both"!')
  }
  
  forward_step <- function(current_mle, mle_fit, criterion) {
    current_model <- current_mle@formula
    current_criterion <- slot(current_mle, criterion)
    # loop over all the terms in the current model, remove one-by-one and keep the one with the
    # the smallest criteria
    terms_current <- attr(terms.formula(current_model), "term.labels")
    terms_full <- attr(terms.formula(mle_fit@formula), "term.labels")
    potential_terms <- which(!(terms_full %in% terms_current))
    best_so_far <- current_mle
    if (length(potential_terms) > 0) {
      for (tl in potential_terms) {
        # new model only with the current tl
        new_terms <- terms.formula(mle_fit@formula)
        current_terms <- attr(terms.formula(current_mle@formula), "term.labels")
        new_formula <- reformulate(c(current_terms, attr(new_terms, "term.labels")[tl]), response = current_model[[2]], intercept = attr(new_terms, "intercept")==1)
        new_fit <- update_mle(mle_fit = mle_fit, 
                              new_formula = new_formula)
        if (is.na(slot(new_fit, criterion))) {
          # try with different starting values
          new_par0 <- c(mle_fit@coefficients$R0, mle_fit@coefficients$others)
          names(new_par0) <- mle_fit@parameter_names
          new_parameters <- attr(new_terms, "term.labels")
          if (attr(new_terms, "intercept") == 1) {
            new_parameters <- c("(Intercept)", new_parameters)
          }
          if (length(mle_fit@coefficients$others) > 0) {
            new_parameters <- c(new_parameters, names(mle_fit@coefficients$others))
          }
          new_par0 <- new_par0[new_parameters]
          new_fit <- update_mle(mle_fit = mle_fit, 
                                new_formula = new_formula,
                                par0 = new_par0)
        }
        if (slot(new_fit, criterion) <= slot(best_so_far, criterion)) {
          best_so_far <- new_fit
        }
      }
    }
    term_added <- setdiff(attr(terms.formula(best_so_far@formula), "term.labels"), 
                          attr(terms.formula(current_mle@formula), "term.labels"))
    lr_stat <- lr_test(current_mle, best_so_far)
    if (length(term_added) > 0) {
      log_line <- data.frame("Added"=term_added, 
                             "Removed"="", 
                             "AIC"=best_so_far@AIC, 
                             "BIC"=best_so_far@BIC,
                             "LR"=attr(lr_stat, "LR"))
      if (verbose) {
        print(log_line)
      }
    }
    else {
      log_line <- data.frame()
    }
    attr(best_so_far, "change") <- log_line
    return(best_so_far)
  }
  
  backward_step <- function(current_mle, mle_fit, criterion, min_terms) {
    current_model <- current_mle@formula
    current_criterion <- slot(current_mle, criterion)
    # loop over all the terms in the current model, remove one-by-one and keep the one with the
    # the smallest criteria
    terms_current <- attr(terms.formula(current_model), "term.labels")
    # we have to keep the terms from the null model
    terms_current <- terms_current[which(!(terms_current %in% min_terms))]
    best_so_far <- current_mle
    if (length(terms_current) > 1) {
      formula_current <- reformulate(terms_current, response=current_model[[2]])
      terms_current <- attr(terms.formula(formula_current), "term.labels")
      for (tl in 1:length(terms_current)) {
        # new model without th current tl
        new_terms <- drop.terms(terms.formula(formula_current), dropx = tl, keep.response = TRUE)
        new_formula <- reformulate(union(min_terms, attr(new_terms, "term.labels")), response = current_model[[2]], intercept = attr(new_terms, "intercept")==1)
        new_fit <- update_mle(mle_fit = mle_fit, 
                              new_formula = new_formula)
        if (is.na(slot(new_fit, criterion))) {
          # try with different starting values
          new_par0 <- c(current_mle@coefficients$R0, current_mle@coefficients$others)
          names(new_par0) <- current_mle@parameter_names
          new_parameters <- attr(new_terms, "term.labels")
          if (attr(new_terms, "intercept") == 1) {
            new_parameters <- c("(Intercept)", new_parameters)
          }
          if (length(current_mle@coefficients$others) > 0) {
            new_parameters <- c(new_parameters, names(current_mle@coefficients$others))
          }
          new_par0 <- new_par0[new_parameters]
          new_fit <- update_mle(mle_fit = mle_fit, 
                                new_formula = new_formula,
                                par0 = new_par0)
        }
        if (slot(new_fit, criterion) <= slot(best_so_far, criterion)) {
          best_so_far <- new_fit
        }
      }
    }
    else if (length(terms_current) ==1) {
      # test the null model
      if (length(min_terms) > 0) {
        new_fit <- update_mle(mle_fit = mle_fit, 
                              new_formula = reformulate(min_terms, 
                                                        response = mle_fit@formula[[2]], 
                                                        intercept = attr(terms.formula(null_model), "intercept")==1))
      }
      else {
        new_fit <- update_mle(mle_fit = mle_fit, 
                              new_formula = formula(paste(mle_fit@formula[[2]], "~ 1", sep="")))
      }
      
      if (is.na(slot(new_fit, criterion))) {
        # try with different starting values
        new_par0 <- c(current_mle@coefficients$R0, current_mle@coefficients$others)
        names(new_par0) <- current_mle@parameter_names
        new_parameters <- attr(new_terms, "term.labels")
        if (attr(new_terms, "intercept") == 1) {
          new_parameters <- c("(Intercept)", new_parameters)
        }
        if (length(current_mle@coefficients$others) > 0) {
          new_parameters <- c(new_parameters, names(current_mle@coefficients$others))
        }
        new_par0 <- new_par0[new_parameters]
        new_fit <- update_mle(mle_fit = mle_fit, 
                              new_formula = new_formula,
                              par0 = new_par0)
      }
      if (slot(new_fit, criterion) <= slot(best_so_far, criterion)) {
        best_so_far <- new_fit
      }
    }
    
    term_removed <- setdiff(attr(terms.formula(current_mle@formula), "term.labels"), 
                            attr(terms.formula(best_so_far@formula), "term.labels"))
    lr_stat <- lr_test(current_mle, best_so_far)
    if (length(term_removed) > 0) {
      log_line <- data.frame("Added"="", 
                             "Removed"=term_removed, 
                             "AIC"=best_so_far@AIC, 
                             "BIC"=best_so_far@BIC,
                             "LR"=attr(lr_stat, "LR"))
      if (verbose) {
        print(log_line)
      }
    }
    else {
      log_line <- data.frame()
    }
    attr(best_so_far, "change") <- log_line
    return(best_so_far)
  }
  
  backward_selection <- function(mle_fit, criterion, min_terms) {
    
    track_changes <- data.frame()
    # repeat the step until the model can be improved
    old_model <- mle_fit
    start_model <- old_model
    new_model <- backward_step(old_model, mle_fit, criterion, min_terms)
    track_changes <- rbind(track_changes, attr(new_model, "change"))
    while (slot(new_model, criterion) != slot(old_model, criterion) || old_model@formula != new_model@formula) {
      old_model <- new_model
      new_model <- backward_step(old_model, mle_fit, criterion, min_terms)
      track_changes <- rbind(track_changes, attr(new_model, "change"))
    }
    attr(new_model, "selection_steps") <- track_changes
    attr(new_model, "start_model") <- start_model@formula
    attr(new_model, "change") <- NULL
    return(new_model)
  }
  
  # forward selection
  forward_selection <- function(mle_fit, criterion, min_terms) {
    track_changes <- data.frame()
    # repeat the step until the model can be improved
    if (length(min_terms)>0) {
      old_model <- update_mle(mle_fit, 
                              new_formula = reformulate(min_terms, 
                                                        response = mle_fit@formula[[2]], 
                                                        intercept = attr(terms.formula(null_model), "intercept")==1))
    }
    else {
      old_model <- update_mle(mle_fit = mle_fit, 
                              new_formula = formula(paste(mle_fit@formula[[2]], "~ 1", sep="")))
    }
    start_model <- old_model
    new_model <- forward_step(old_model, mle_fit, criterion)
    track_changes <- rbind(track_changes, attr(new_model, "change"))
    while (slot(new_model, criterion) != slot(old_model, criterion) || old_model@formula != new_model@formula) {
      old_model <- new_model
      new_model <- forward_step(old_model, mle_fit, criterion)
      track_changes <- rbind(track_changes, attr(new_model, "change"))
    }
    attr(new_model, "selection_steps") <- track_changes
    attr(new_model, "start_model") <- start_model@formula
    attr(new_model, "change") <- NULL
    return(new_model)
  }
  
  # combined selection
  two_way_selection <- function(mle_fit, criterion, min_terms) {
    track_changes <- data.frame()
    # repeat the step until the model can be improved
    old_model <- mle_fit
    start_model <- old_model
    new_model <- backward_step(old_model, mle_fit, criterion, min_terms)
    track_changes <- rbind(track_changes, attr(new_model, "change"))
    while (slot(new_model, criterion) != slot(old_model, criterion) || old_model@formula != new_model@formula) {
      old_model <- new_model
      new_model_1 <- backward_step(old_model, mle_fit, criterion, min_terms)
      new_model_2 <- forward_step(old_model, mle_fit, criterion)
      if (slot(new_model_1, criterion) < slot(new_model_2, criterion)) {
        new_model <- new_model_1
      }
      else {
        new_model <- new_model_2
      }
      track_changes <- rbind(track_changes, attr(new_model, "change"))
    }
    attr(new_model, "selection_steps") <- track_changes
    attr(new_model, "start_model") <- start_model@formula
    attr(new_model, "change") <- NULL
    return(new_model)
    
  }
  
  min_terms <- attr(terms.formula(null_model), "term.labels")
  
  if (direction=="backward") {
    return(backward_selection(mle_fit, criterion, min_terms))
  }
  else if (direction=="forward") {
    return(forward_selection(mle_fit, criterion, min_terms))
  }
  else {
    return(two_way_selection(mle_fit, criterion, min_terms))
  }
}