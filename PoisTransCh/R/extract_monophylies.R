extract_monophylies <- function(tree, node_groups, group) {
  
  # tree: phylogenetic tree
  # nodeGroups: group to which each tip belongs to
  # group: group of the tips of interest (its'name)
  
  # to start: which are the tips of interest?
  ind <- which(node_groups %in% group)
  seqs <- names(node_groups)[ind]
  tipnr <- NULL
  wCherries <- which(tree$tip.label %in% seqs)
  
  edges <- as.data.frame(tree$edge)
  names(edges) <- c("ancestor", "descendant")
  row.names(edges) <- seq(1:nrow(edges))
  
  # identifying the MCRA of the transmission chains
  while (!setequal(wCherries, tipnr)) {
    tipnr <- wCherries
    focus.edges <- edges[which(edges$descendant %in% tipnr),,drop=FALSE]
    wCherries <- by(focus.edges, INDICES = focus.edges$ancestor,FUN = function(df) {
      cherry <- nrow(df) >= 2
      if (cherry) {
        return(unique(df$ancestor))
      }
      else {
        return(unique(df$descendant))
      }
    })
    wCherries <- as.numeric(unlist(wCherries))
  }
  
  N <- length(tree$tip.label)
  
  # getting the sequences from the chains
  chains <- lapply(wCherries, FUN=function(node) {
    if (node <= N) {
      return(tree$tip.label[node])
    }
    else {
      return(ape::extract.clade(tree, node = node)$tip.label)
    }
  })
  names(chains) <- wCherries
  return(chains)
}