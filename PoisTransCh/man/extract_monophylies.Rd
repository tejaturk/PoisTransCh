\name{extract_monophylies}
\alias{extract_monophylies}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
Extract the monophyletic groups from a phylogeny
}
\description{
This function identifies the transmission clusters in a tree, in which each node of the cluster belongs to one of the groups of interest. These clusters represent the monophylies with respect to the specified grouping. It is a special case of \code{"extract_clusters"} with threshold being 1, but much faster.
}
\usage{
extract_monophylies(tree, node_groups, group)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{tree}{Object of class \code{"phylo"} - pyhlogenetic tree containing the sequences of interest and other background sequences.
}
  \item{node_groups}{Object of class \code{"character"} - a vector indicating the group/class each tip in the \code{tree} belongs to. Its length should equal the number of tips in the phylogenetic tree.
}
  \item{group}{Object of class \code{"group"} - a vector (or a single name) with names of groups of interest. All the tips belonging to one of the groups indicated in \code{group} are considered for forming monophylies.
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
A \code{"list"} of monophylies of the tree where each element represents a monophyly and its element the tips forming it. The names of the elements correspond to the nodes numbers (for monophylies bigger than 1) or tip numbers (for singletones).
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
Teja Turk
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
\code{\link{extract_clusters}}
}
\examples{
set.seed(346)
# simulate a random phylogenetic tree
phylo_tree <- ape::rtree(n = 100, 
                         rooted = FALSE)
# randomly assign the groups to the tips
all_classes <- c("foreign", "other risk", "heterosexual")
probs <- c("foreign" = 0.15, 
           "other_risk" = 0.30,
           "heterosexual" = 0.55)
colors <- c("foreign" = "darkgray", 
            "other risk" = "blue",
            "heterosexual" = "red")
tips_types <- sample(all_classes,
                     size = 100,
                     prob = probs,
                     replace = TRUE)
names(tips_types) <- phylo_tree$tip.label                     
# plot the phylogeny
plot(phylo_tree, cex = 0.5, font = 2,
     tip.color = colors[tips_types[phylo_tree$tip.label]])

# extract the heterosexuals transmission chains
het_chains <- extract_monophylies(tree = phylo_tree,
                                  node_groups = tips_types[phylo_tree$tip.label],
                                  group = "heterosexual")
# transmission chain sizes                                
unlist(lapply(het_chains, length))  

# extract the domestic transmission chains
domestic_chains <- extract_monophylies(tree = phylo_tree,
                                       node_groups = tips_types[phylo_tree$tip.label],
                                       group = c("heterosexual", "other risk"))
# transmission chain sizes                                
unlist(lapply(domestic_chains, length))  
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{monophyly}
\keyword{cluster}
