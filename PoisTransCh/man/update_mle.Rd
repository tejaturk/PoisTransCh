\name{update_mle}
\docType{methods}
\alias{update_mle}
\title{Update MLE fit with a new formula}
\description{
The function fits the transmission chains data with the updated formula. The remaining model parameters remain the same as in the original fit.
}
\arguments{
  \item{mle_fit}{Object of class \code{"MLEfit"}: object constructed by the function \code{mle} during the original maximum likelihood estimation.
}
  \item{new_formula}{Object of class \code{"formula"} - a new formula describing the relation between the transmission chain characteristics and the basic reproductive number \eqn{R_{0}}{R_0}.
}
  \item{par0}{Object of class \code{"numeric"} - (optionally) a vector of initial values for the updated model. If missing initial values are provided by the function \code{\link{init_R0}} or \code{\link{init_R0_reg_coef}}.
}
}
\section{Methods}{
\describe{

\item{\code{signature(mle_fit = "MLEfit", new_formula = "formula", par0 = "missing")}}{
%%  ~~describe this method here~~
}

\item{\code{signature(mle_fit = "MLEfit", new_formula = "formula", par0 = "numeric")}}{
%%  ~~describe this method here~~
}
}}
\value{
An \code{"MLEfit"} object of the updated maximum likelihood estimate.
}
\author{
Teja Turk
}
\examples{
# original fit
simple_fit <- mle(chain_size~1, 
                  data = transchdata,
                  p = transchdata$p,
                  rho = transchdata$rho,
                  model = "Poisson",
                  tol_R0 = 0.05)
simple_fit@formula
simple_fit@AIC

# updated model - adding the subtype as a covariate
updated_fit <- update_mle(mle_fit = simple_fit,
                          new_formula = chain_size~subtype)
updated_fit@formula
updated_fit@AIC
}
\keyword{update}
\keyword{maximum likelihood estimate}
